package com.aeq;

import java.util.ArrayList;
import java.util.List;

public class AccessMainmethod {
	public static void main(String[] args) {
		// String []n={"1","2","3"};
		// ArrayListOfArrays.main(n);
		getMethod();
	}

	public static void getMethod() {
		List[] n = new ArrayList[2];
		for (int i = 0; i < 2; i++)
			n[i] = new ArrayList();
		A a = new A();
		a.setName("Princya");
		n[0].add(a);
		B b = new B();
		b.setId(345848);
		n[1].add(b);
		for (Object o : n) {
			System.out.println(o);
		}
	}
}

class A {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.name;
	}
}

class B {
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.id + " ";
	}

}