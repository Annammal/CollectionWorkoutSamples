package com.aeq;
/*class MyOuterClassDemo 
{
 private int x= 1; 
public void doThings()
{
 String name ="local variable";
 // inner class defined inside a method of outer class class MyInnerClassDemo 
{ 
public void seeOuter () 
{
 System.out.println("Outer Value of x is :" + x);
 System.out.println("Value of name is :" + name);
//compilation error!! } //close inner class method
 } 
// close inner class definition
 } 
//close Top level class method 
}*/
class SampleDemo 
{
private int x= 1;

public void doThings()
{ 
	String name ="local variable"; 
// inner class defined inside a method of outer class 
	 class MyInnerClassDemo {

	public void seeOuter () 
	{ 
	System.out.println("Outer Value of x is :" + x);
	System.out.println("Value of name is :" + name);
	//compilation error!!
	}
	//close inner class method
	
// close inner class definition 
//close Top level class method
}
}
}
public class MyOuterClassDemo{
	public static void main(String[] args) {
		SampleDemo sample=new SampleDemo();
		
	}
}