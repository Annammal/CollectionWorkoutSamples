package com.aeq;

import java.util.Comparator;

public class Asendingbean{
String name;

@Override
public String toString() {
	return "Asendingbean [name=" + name + "]";
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((name == null) ? 0 : name.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Asendingbean other = (Asendingbean) obj;
	if (name == null) {
		if (other.name != null)
			return false;
	} else if (!name.equals(other.name))
		return false;
	return true;
}


	// TODO Auto-generated method stub
}

class MyNameComp implements Comparator<Asendingbean>
{ 
	@Override 
	public int compare(Asendingbean e1, Asendingbean e2) 
{ 
		return e1.getName().compareTo(e2.getName()); 
} 
} 
