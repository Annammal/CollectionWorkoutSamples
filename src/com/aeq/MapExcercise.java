package com.aeq;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

class EmployeDetails{
	String name;
	String position;
	int sal;
	public EmployeDetails(String name, String position, int sal) {
		//super();
		this.name = name;
		this.position = position;
		this.sal = sal;
	}
	
}
public class MapExcercise {
	
	public static void main(String[] args) 
	{ 
		EmployeDetails employe1=new EmployeDetails("Murugan","JuniorDeveloper",5000);
		EmployeDetails employe2=new EmployeDetails("Balan","JuniorDeveloper",5000);
		EmployeDetails employe3=new EmployeDetails("Elavarasu","SeniorDeveloper",10000);
		
		//Map<Integer,EmployeDetails> mapobj=new HashMap<Integer,EmployeDetails>();
		Map<Integer,EmployeDetails> mapobj=new ConcurrentHashMap<Integer,EmployeDetails>();
		mapobj.put(4,employe1);
		mapobj.put(2,employe2);
		mapobj.put(3,employe3);
		
		
	
	/*	Set set=mapobj.entrySet();
		Iterator iterator=set.iterator();
		System.out.println(mapobj);
		while(iterator.hasNext()){
			Entry entry=(Entry) iterator.next();
			Integer n=(Integer) entry.getKey();
			EmployeDetails vaDetails=(EmployeDetails) entry.getValue();
			if(vaDetails.sal>=10000)
			{
				System.out.println("This is worst : "+vaDetails.name+" "+entry.getKey());
				iterator.remove();
			}
			
		}
		System.out.println(mapobj);
		*/
	
		//Using ConcurrentHashMap its work
		
		for(Map.Entry<Integer,EmployeDetails> entry:mapobj.entrySet())
		{
			Integer key=entry.getKey();
			//System.out.println();
			EmployeDetails values=entry.getValue();
		//	System.out.println("Employe Details no : "+key);
			//System.out.println(values.name+" "+values.position+" "+values.sal);
			
			if(values.sal>=10000)
			{
				//Map.Entry 
				System.out.println("This is worst : "+values.name+" "+entry.getKey());
				 Integer pointIndex = entry.getKey();
				 //mapobj.put(pointIndex, values.name);
				mapobj.remove(pointIndex);
			}
			//System.out.println(values.name+" "+values.position+" "+values.sal);
		}
	System.out.println(mapobj);
		
	}
}