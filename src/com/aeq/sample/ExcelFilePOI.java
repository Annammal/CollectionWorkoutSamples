package com.aeq.sample;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.aeq.excel.HTTPClientTest;
import com.aeq.excel.SPARQLResponse;

/**
 * Servlet implementation class ReadExcel
 */
public class ExcelFilePOI extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String temp_path = "/home/aequalis/Documents/Annam/";
	private String file_path = "";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ExcelFilePOI() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		/*
		 * Sample Java n = new SampleJava();
		 */

		PrintWriter printWriter = response.getWriter();

		// request.setAttribute("value", n.printval());
		RequestDispatcher rd = request.getRequestDispatcher("output.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter pWriter = response.getWriter();
		pWriter.println("Server Post");
		if (ServletFileUpload.isMultipartContent(request)) {
			try {
				List<FileItem> multiparts = new ServletFileUpload(
						new DiskFileItemFactory()).parseRequest(request);
				String file_path = "";
				for (FileItem item : multiparts) {
					if (!item.isFormField()) {
						String name = new File(item.getName()).getName();
						file_path = temp_path + name;
						item.write(new File(file_path));
					}
				}
				/* SampleJava filevalue=new SampleJava(); */
				List<SPARQLResponse> listVals = new ArrayList<SPARQLResponse>();
				List ls = printval(file_path);

				request.setAttribute("value", ls);
			} catch (Exception ex) {
				request.setAttribute("message", "File Upload Failed due to "
						+ ex);
			}

		} else {
			request.setAttribute("message",
					"Sorry this Servlet only handles file upload request");
		}

		RequestDispatcher rd = request.getRequestDispatcher("output.jsp");
		rd.forward(request, response);

	}

	public List<SPARQLResponse> printval(String excelFilePath) throws IOException, URISyntaxException {

		List<SPARQLResponse> listVals = new ArrayList<SPARQLResponse>();
		FileInputStream excelFile = new FileInputStream(new File(excelFilePath));
		Workbook workbook = new XSSFWorkbook(excelFile);
		Sheet datatypeSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = datatypeSheet.iterator();
		
		/*File file = new File("/home/user/file.txt");
		try {
		    List<String> lines = FileUtils.readLines(file);
		} catch (IOException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}*/

		double n = 0;

		while (iterator.hasNext()) {

			Row currentRow = iterator.next();
			int rownum = currentRow.getRowNum();
			if (rownum == 4){
				currentRow = iterator.next();
				Iterator<Cell> cellIterator = currentRow.iterator();
				while (cellIterator.hasNext()) {
					SPARQLResponse bval = new SPARQLResponse();
					Cell currentCell = cellIterator.next();
					String name = currentCell.toString();
					
					if(name!=null){
						String urlName = name.substring(name.lastIndexOf('/')+1);
						SPARQLResponse response = HTTPClientTest.getdbpediaInfo(urlName);
						listVals.add(response);
					}
				}
			}
		
		}
		return listVals;
	}
/*public static void main(String[] args) {
	String url="http://dbpedia.org/page/Dr._Dre";
	
	System.out.println(url.lastIndexOf('/'));
	System.out.println(url.substring(url.lastIndexOf('/')+1));
}*/
}
