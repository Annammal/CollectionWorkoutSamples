package com.aeq.sample;

import java.io.IOException;

import org.xml.sax.SAXException;

import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebForm;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebResponse;

public class HttpUnitSample {
public static void main(String[] args) throws IOException, SAXException {
	String forName="Lil Wayne";
	String queryString = "PREFIX dbpo: <http://dbpedia.org/ontology/> "
			+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
			+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
			+ "SELECT distinct "
			+ "?personName "
			+ "?birthDate "
			+ "(group_concat (?countryname;separator=', ') as ?countryTitle) "
			+ "(group_concat (?birthplacename;separator=', ') as ?nameofBirthPlace) "
			+ "(group_concat (?hometownname;separator=', ') as ?nameofHomeTown) "
			+ "(group_concat (?occupationTitle;separator=', ') as ?occupations) "
			+ "?isAlive "
			+ "WHERE { "
			+ "?person rdfs:label'"+forName+"'@en ."
			+ "?person rdf:type dbpo:Person ."
			+ "OPTIONAL {?person rdfs:label ?personName FILTER (lang(?personName) = 'en')}"
			+ "OPTIONAL {?person dbpo:birthDate ?birthDate }"
			+ "OPTIONAL {?person dbpo:country ?country. ?country rdfs:label ?countryname FILTER(lang(?countryname)='en') }"
			+ "OPTIONAL {?person dbpo:birthPlace ?birthPlace. ?birthPlace rdfs:label ?birthplacename FILTER (lang(?birthplacename) = 'en') }"
			+ "OPTIONAL {?person dbpo:hometown ?hometown. ?hometown rdfs:label ?hometownname  FILTER (lang(?hometownname) = 'en') }"
			+ "OPTIONAL {?person dbpo:deathDate ?deathDate }"
			+ "OPTIONAL {?person dbpo:occupation ?occupation . ?occupation dbpo:title ?occupationTitle}"
			+ "bind( if( bound(?deathDate), 'No', 'Yes') as ?isAlive)"
			+ "} GROUP BY ?personName ?birthDate ?isAlive";
	System.out.println(queryString);
	 WebConversation     conversation = new WebConversation();
     WebRequest  request = new GetMethodWebRequest( 
         "http://dbpedia.org/sparql" );
     WebResponse response = conversation.getResponse( request );
     WebForm loginForm = response.getForms()[0];
     request = loginForm.getRequest();
     request.setParameter( "query", queryString );
     response = conversation.getResponse( request );
     System.out.println(response);
}
}
