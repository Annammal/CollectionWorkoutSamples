package com.aeq.sample;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

public class FederatedQuery {

	public static SPARQLResponse getdbpediaInfo(String forName) {
		
		SPARQLResponse response = new SPARQLResponse();
		String queryString = "PREFIX dbpo: <http://dbpedia.org/ontology/> "
				+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
				+ "SELECT distinct "
				+ "?personName "
				+ "?birthDate "
				+ "(group_concat (?countryname;separator=', ') as ?countryTitle) "
				+ "(group_concat (?birthplacename;separator=', ') as ?nameofBirthPlace) "
				+ "(group_concat (?hometownname;separator=', ') as ?nameofHomeTown) "
				+ "(group_concat (?occupationTitle;separator=', ') as ?occupations) "
				+ "?isAlive "
				+ "WHERE { "
				+ "?person rdfs:label'"+forName+"'@en ."
				+ "?person rdf:type dbpo:Person ."
				+ "OPTIONAL {?person rdfs:label ?personName FILTER (lang(?personName) = 'en')}"
				+ "OPTIONAL {?person dbpo:birthDate ?birthDate }"
				+ "OPTIONAL {?person dbpo:country ?country. ?country rdfs:label ?countryname FILTER(lang(?countryname)='en') }"
				+ "OPTIONAL {?person dbpo:birthPlace ?birthPlace. ?birthPlace rdfs:label ?birthplacename FILTER (lang(?birthplacename) = 'en') }"
				+ "OPTIONAL {?person dbpo:hometown ?hometown. ?hometown rdfs:label ?hometownname  FILTER (lang(?hometownname) = 'en') }"
				+ "OPTIONAL {?person dbpo:deathDate ?deathDate }"
				+ "OPTIONAL {?person dbpo:occupation ?occupation . ?occupation dbpo:title ?occupationTitle}"
				+ "bind( if( bound(?deathDate), 'No', 'Yes') as ?isAlive)"
				+ "} GROUP BY ?personName ?birthDate ?isAlive";

		Query query = QueryFactory.create(queryString);
		QueryExecution q = QueryExecutionFactory.sparqlService("http://dbpedia.org/sparql", query);
		
		try {
			ResultSet result = q.execSelect();
			if (result.hasNext()) {
				QuerySolution solution = result.next();
				if (solution.get("personName") != null){
					response.setName(solution.getLiteral("personName").getLexicalForm());
				}else{
					response.setName("NA");
				}
				if (solution.get("birthDate") != null){
						response.setBirthDate(solution.getLiteral("birthDate").getLexicalForm());
				}else{
					response.setBirthDate("NA");
				}
				if (solution.get("countryTitle") != null){
							response.setCountry(solution.get("countryTitle").toString());
				}
				else{
					response.setCountry("NA");
				}
				if (solution.get("nameofBirthPlace") != null){
								response.setBirthPlace(solution.get("nameofBirthPlace").toString());
				}else{
					response.setBirthPlace("NA");
				}
				if (solution.get("nameofHomeTown") != null){
								response.setHomeTown(solution.get("nameofHomeTown").toString());
				}else{
					response.setHomeTown("NA");
				}
				if (solution.get("occupations") != null){
									response.setOccupations(solution.get("occupations").toString());
				}else{
					response.setOccupations("NA");
				}
				if (solution.get("isAlive") != null){
										response.setAliveStatus(solution.get("isAlive").toString());
				
				}else{
					response.setAliveStatus("NA");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			q.close();
		}
		//System.out.println(response);
		return response;
		
	}
	
	public static void main(String[] args) {
		FederatedQuery.getdbpediaInfo("Lil Wayne");
	}
}

