package com.aeq;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.perf4j.StopWatch;
import org.perf4j.log4j.Log4JStopWatch;

public class CollectionsClassEx {
	private final static Logger logger = Logger.getLogger(CollectionsClassEx.class);
	
	
	public void addAllEx() throws InterruptedException{
		StopWatch stopWatch = new Log4JStopWatch("email");
		Thread.sleep((long)(Math.random() * 1000L));
		List<Integer> s1=new LinkedList<Integer>();
		s1.add(10);
		s1.add(9);
		s1.add(8);
		s1.add(7);
		s1.add(6);
		System.out.println("before addAll:"+s1);
		Collections.addAll(s1, 1,2,3,4,5);
		System.out.println("after addAll:"+s1);
		logger.info("email");
		stopWatch.stop();
	}
	public static void suffleEx(){
		List<Integer> s1=new LinkedList<Integer>();
		s1.add(1);
		s1.add(2);
		s1.add(3);
		s1.add(4);
		s1.add(5);
		System.out.println("Before suffle:"+s1);
		Collections.shuffle(s1);
		System.out.println("after suffle:"+s1);
	}
	
	public static void binarySearch(){
		ArrayList<String> s=new ArrayList<String>();
		s.add("pavi");
		s.add("praisee");
		s.add("princy");
		int index=Collections.binarySearch(s, "princy");
		System.out.println("index position of princy in list: "+index);
	}
	
	public static void copy(){
		ArrayList<String> s=new ArrayList<String>();
		ArrayList<String> s1=new ArrayList<String>(4);
		s.add("java");
		s.add("c");
		s.add("c++");
		
		s1.add("php");
		s1.add("c#");
		s1.add("python");
		s1.add("visual basic");
		Collections.copy(s1, s);
		System.out.println(s1);
	}
	
	public static void disjoint(){
		 List<String> srclst = new ArrayList<String>();
		   List<String> destlst = new ArrayList<String>();
		   srclst.add("Java");
		   srclst.add("is");
		   srclst.add("best");
		   
		   destlst.add("Javaqqq");
		   destlst.add("C++");
		   destlst.add("is not");
		   destlst.add("older"); 
		   boolean isCommon=Collections.disjoint(srclst, destlst);
		   System.out.println(isCommon);
	}
	
	public static void emptyList(){
		List<String> srclst = new ArrayList<String>();
		 List emptylst = Collections.emptyList();
		 System.out.println(srclst);
		 System.out.println(emptylst);
	}
	
	public static void fill(){
		List<String> srclst = new ArrayList<String>();
		 srclst.add("Java");
		   srclst.add("is");
		   srclst.add("best");
		   System.out.println("before fill: "+srclst);
		   Collections.fill(srclst, "programing language");
		   System.out.println("after fill: "+srclst);
	}
	
	public static void count(){
		List<String> val=new ArrayList<String>();
		val.add("liyana");
		val.add("liyana");
		val.add("Lilly");
		val.add("Lilly");
		val.add("Lilly");
		int countString=Collections.frequency(val, "liyana");
		System.out.println("String Count: "+countString);
		List<Character> ch=new ArrayList<Character>();
		ch.add('a');
		ch.add('g');
		ch.add('d');
		ch.add('a');
		ch.add('r');
		ch.add('d');
		ch.add('a');
		int count=Collections.frequency(ch, 'a');
		System.out.println("no of occurance: "+count);
	}
	
	public static void lastIndexOf(){
		List<String>arrlistsrc = new ArrayList<String>();
		List<String> arrlisttarget = new ArrayList<String>();
		      
		   arrlistsrc.add("A");
		   arrlistsrc.add("B");
		   arrlistsrc.add("C");
		   arrlistsrc.add("D");
		   arrlistsrc.add("E"); 
		  
		   arrlisttarget.add("C");
		   arrlisttarget.add("D");
		   arrlisttarget.add("E");
		   int index = Collections.lastIndexOfSubList(arrlistsrc, arrlisttarget);
		      
		   System.out.println("Starting position is: "+index);
	}
	
	public static void maxElement(){
		List<Integer> arrlistsrc = new ArrayList<Integer>();
		arrlistsrc.add(-12);
		arrlistsrc.add(34);
		arrlistsrc.add(-30);
		arrlistsrc.add(10);
		int m=Collections.max(arrlistsrc);
		System.out.println(m);
	}
	
	public static void minElement(){
		List<Integer> arrlistsrc = new ArrayList<Integer>();
		arrlistsrc.add(-12);
		arrlistsrc.add(34);
		arrlistsrc.add(30);
		arrlistsrc.add(10);
		int m=Collections.min(arrlistsrc);
		System.out.println(m);
	}
	
	public static void replaceAll(){
		List<Integer> arrlistsrc = new ArrayList<Integer>();
		arrlistsrc.add(-12);
		arrlistsrc.add(34);
		arrlistsrc.add(30);
		arrlistsrc.add(-12);
		System.out.println("before replacment"+arrlistsrc);
		Collections.replaceAll(arrlistsrc, -12, 12);
		System.out.println("after replacment:"+arrlistsrc);
	}
	
	public static void reverse(){
		List<Integer> arrlistsrc = new ArrayList<Integer>();
		arrlistsrc.add(2);
		arrlistsrc.add(34);
		arrlistsrc.add(30);
		arrlistsrc.add(1);
		System.out.println("before reverse"+arrlistsrc);
		Collections.reverse(arrlistsrc);
		System.out.println("after reverse:"+arrlistsrc);
	}
	
	public static void sort(){
		List<Integer> arrlistsrc = new ArrayList<Integer>();
		arrlistsrc.add(2);
		arrlistsrc.add(34);
		arrlistsrc.add(30);
		arrlistsrc.add(1);
		System.out.println("before sort"+arrlistsrc);
		Collections.sort(arrlistsrc);
		System.out.println("after sort:"+arrlistsrc);
	}
	public static void swap(){
		List<Integer> arrlistsrc = new ArrayList<Integer>();
		arrlistsrc.add(2);
		arrlistsrc.add(34);
		arrlistsrc.add(30);
		arrlistsrc.add(1);
		System.out.println("before swap"+arrlistsrc);
		Collections.swap(arrlistsrc, 2, 0);
		System.out.println("after swap:"+arrlistsrc);
	}
	
	public static void unmodifiableCollection(){
		List<Integer> arrlistsrc = new ArrayList<Integer>();
		arrlistsrc.add(2);
		arrlistsrc.add(34);
		arrlistsrc.add(30);
		arrlistsrc.add(1);
		System.out.println("before "+arrlistsrc);
		Collection<Integer> col=Collections.unmodifiableCollection(arrlistsrc);
		col.add(50);
		System.out.println("after:"+col);
	}
public static void main(String[] args) throws InterruptedException {
	logger.info("Welcome to Perf4j");
CollectionsClassEx collectionsClassEx=new CollectionsClassEx();
	collectionsClassEx.addAllEx();
	//binarySearch();
	//suffleEx();
	//copy();
	//disjoint();
	//emptyList();
	//fill();
	//count();
	//lastIndexOf();
	//maxElement();
	//minElement();
	//replaceAll();
	//reverse();
	//sort();
	//swap();
	//unmodifiableCollection();
	}
}