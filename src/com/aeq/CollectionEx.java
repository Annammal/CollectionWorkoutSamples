package com.aeq;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;


public class CollectionEx {
	static int getCapacity(ArrayList<?> l) throws Exception {
        Field dataField = ArrayList.class.getDeclaredField("elementData");
        dataField.setAccessible(true);
        return ((Object[]) dataField.get(l)).length;
    }

	public static void main(String[] args) throws Exception {
		
	/*	Student student1=new Student("Maha",22,"Kerala");
		Student student2=new Student("Aruna",24,"TN");
		Student student3=new Student("Prince",25,"Delli");
		
		Student student4=new Student("Maha",22,"Kerala");
		Student student5=new Student("Aruna",24,"TN");
		Student sud=new Student();
		

		List list=new ArrayList();		
		list.add(student1);
		list.add(student2);
		list.add(student3);
		
		List<Student> students=new ArrayList<Student>();
		students.add(student4);
		students.add(student5);
		
		for(Student s:students){
			System.out.println(s.name+s.age+s.state);
		}

		Iterator iterator=list.iterator();
		while(iterator.hasNext()){
			Student val=(Student) iterator.next();
			//System.out.println(val);
			//System.out.println("name:"+val.getName() + " Age: "+val.getAge() +" State: "+val.getState());
		}
		
		Map map=new HashMap();
		map.put(1, "Sara");
		map.put(2, "Glory");
		
		
		
		
		ArrayList arrayList=new ArrayList();
		arrayList.add("mur'ugan");
		arrayList.add("Balu");
		arrayList.add("Elango");
		
		
		List newlist=new ArrayList();
		
		newlist.add("Gopal");
		newlist.add("Anand");
		newlist.add("James");
		System.out.println("Before remove method"+newlist);
		newlist.remove("Anand");
		arrayList.add(newlist);
		
		System.out.println("After remove Method:"+newlist);
		
		Iterator itr=arrayList.iterator();
		while(itr.hasNext()){
			
			System.out.println(itr.next());
			
		}*/
	
	/*	ArrayList arrayList=new ArrayList();
		arrayList.add("mur'ugan");
		arrayList.add("Balu");
		arrayList.add("Elango");
		arrayList.add("xdcfgvhjnmkl,");
		
         Collections.sort(arrayList);
        // Collections.swap(arrayList, 1, 2);
        // System.out.println(Collections.max(arrayList));
		
		ListIterator listIterator=arrayList.listIterator();

		//System.out.println("Forward Iteration");
		while(listIterator.hasNext()){
			//System.out.println(listIterator.next());
		}
		//System.out.println("Backward Iteration");
		while (listIterator.hasPrevious()) {
			//System.out.println(listIterator.previous());
			
		}*/
	
		//Iterator iterator1=arrayList.iterator();

		/*for(Object n:arrayList){
		//	System.out.println(n);
		}*/

		/*HashSet hashSet1=new HashSet();
		//System.out.println(hashSet1.size());
		hashSet1.add("India");
		hashSet1.add("US");
		hashSet1.add("Australia");*/
		
		//System.out.println(hashSet1.size());
		
	/*HashSet hashSet=new HashSet();
	hashSet.add("India");
	hashSet.add("US");
	hashSet.add("Australia");
	hashSet.add("India");
	Iterator iterator=hashSet.iterator();
	while (iterator.hasNext()) {
		System.out.println(iterator.next());
		
	}*/
		/*ArrayList arrayList=new ArrayList(10);
		System.out.println(arrayList.size());
		arrayList.add("mur'ugan");
		arrayList.add("Balu");
		arrayList.add("Elango");
		arrayList.trimToSize();
		arrayList.add("cgd");*/
		//arrayList.ensureCapacity(20);
		 ArrayList<Integer> list = new ArrayList<Integer>(12);
	        for (int i = 0; i < 17; i++) {
	            list.add(i);
	            System.out.format("Size: %2d, Capacity: %2d%n",
	                              list.size(), getCapacity(list));
	        }
	       // list.ensureCapacity(10);
	        list.add(100);
		 System.out.format("Size: %2d, Capacity: %2d%n",
				 list.size(), getCapacity(list));		
	/*	BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(System.in));
		Set<SampleBean> treeSet = new TreeSet<SampleBean>();
		String na = "";
		SampleBean sample = null;
		for (int i = 0; i < 2; i++) {
			sample = new SampleBean();
			System.out.println("Enter name");
			na = bufferedReader.readLine();
			sample.setName(na);
			treeSet.add(sample);
		}
		System.out.println(treeSet);
		for (SampleBean s : treeSet) {
			System.out.println("Entered Name:");
			System.out.println(s.getName());
		}
	*/
}
}
/*class Student {
	String name;
	int age;
	String state;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Student(String name, int age, String state) {
		super();
		this.name = name;
		this.age = age;
		this.state = state;
	}
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}
	

}*/
class SampleBean implements Comparable {
	String name;
	String frdName;

	public String getFrdName() {
		return frdName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((frdName == null) ? 0 : frdName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SampleBean other = (SampleBean) obj;
		if (frdName == null) {
			if (other.frdName != null)
				return false;
		} else if (!frdName.equals(other.frdName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public void setFrdName(String frdName) {
		this.frdName = frdName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

}
