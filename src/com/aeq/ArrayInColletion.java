package com.aeq;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayInColletion {
public static void main(String[] args) {
	
	List<Integer> stock=new ArrayList<Integer>();
	stock.add(54);
	stock.add(23);
	stock.add(33);
	
	Integer[] val=new Integer[stock.size()];
	
	val=stock.toArray(val);
	
	for(Integer obj:val){
		System.out.println(obj);
	}
	
	List<String> stockList = new ArrayList<String>();
	stockList.add("stock1");
	stockList.add("stock2");

	String[] stockArr = new String[stockList.size()];
	stockArr = stockList.toArray(stockArr);

	for(String s : stockArr)
	    System.out.println(s);
}
}
