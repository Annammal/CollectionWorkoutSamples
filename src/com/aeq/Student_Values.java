package com.aeq;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.Map.Entry;

public class Student_Values {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);

		// Set<Student> set = new HashSet<Student>();

		List_student ls;
		ls = new List_student();
		List<Student1> listsize=new ArrayList<Student1>();
		listsize=ls.l1();
		Iterator<Student1> i = listsize.iterator();
		System.out.println(listsize.size());
		
		//Integer n=ls.l1().size();	
		
		System.out.println("Get value using array List:");
		System.out
				.println("--------------------------------------------------");
		while (i.hasNext()) {
			Student1 s2 = (Student1) i.next();
			System.out.println("Student id:" + s2.getStudent_id()
					+ "\nStudent_Name:" + s2.getStudent_name() + "\nAge:"
					+ s2.getAge() + "\nYear of passing:" + s2.getYear()
					+ "\nDepartment:" + s2.getDept());
			System.out
					.println("###########################################################################");

		}
	//System.out.println(n);
		
		
		

		
	}
}

class Student1 {
String Student_id;
String Student_name;
int age;
String dept;
int year;
public Student1(){
	
}
public Student1(String student_id, String student_name, int age, String dept,
		int year) {
	super();
	Student_id = student_id;
	Student_name = student_name;
	this.age = age;
	this.dept = dept;
	this.year = year;
}

public void setStudent_id(String student_id2) {
	Student_id = student_id2;
}
public String getStudent_name() {
	return Student_name;
}
public void setStudent_name(String student_name) {
	Student_name = student_name;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public String getDept() {
	return dept;
}
public void setDept(String dept) {
	this.dept = dept;
}
public int getYear() {
	return year;
}
public void setYear(int year) {
	this.year = year;
}
@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result
			+ ((Student_id == null) ? 0 : Student_id.hashCode());
	/*result = prime * result
			+ ((Student_name == null) ? 0 : Student_name.hashCode());
	result = prime * result + age;
	result = prime * result + ((dept == null) ? 0 : dept.hashCode());
	result = prime * result + year;*/
	return result;
}
@Override
public String toString() {
	return "Student [Student_id=" + Student_id + ", Student_name="
			+ Student_name + ", age=" + age + ", dept=" + dept + ", year="
			+ year + "]";
}
public String getStudent_id() {
	return Student_id;
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Student other = (Student) obj;
	if (Student_id == null) {
		if (other.Student_id != null)
			return false;
	} 
	else if (!Student_id.equals(other.Student_id))
		return false;
	/*if (Student_name == null) {
		if (other.Student_name != null)
			return false;
	} else if (!Student_name.equals(other.Student_name))
		return false;
	if (age != other.age)
		return false;
	if (dept == null) {
		if (other.dept != null)
			return false;
	} else if (!dept.equals(other.dept))
		return false;
	if (year != other.year)
		return false;*/
	return true;
}

}